﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;

namespace statistiker
{
    public class Unterrichts : List<Unterricht>
    {
        public object Wert { get; private set; }

        public Unterrichts()
        {
        }

        public Unterrichts(string aktSj, DateTime datumErsterTagDesPrüfZyklus, string connectionString, int periode, Klasses klasses, Lehrers lehrers, Fachs fachs, Raums raums, Unterrichtsgruppes unterrichtsgruppes)
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(connectionString))
            {
                int id = 0;

                try
                {
                    string queryString = @"SELECT DISTINCT 
Lesson_ID,
LessonElement1,
Periods,
Lesson.LESSON_GROUP_ID,
Lesson_TT,
Flags,
DateFrom,
DateTo
FROM LESSON
WHERE (((SCHOOLYEAR_ID)= " + aktSj + ") AND ((TERM_ID)=" + periode + ") AND ((Lesson.SCHOOL_ID)=177659) AND (((Lesson.Deleted)=No))) ORDER BY LESSON_ID;";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    Console.WriteLine("Unterrichte");
                    Console.WriteLine("-----------");

                    while (oleDbDataReader.Read())
                    {
                        id = oleDbDataReader.GetInt32(0);
                        
                        string wannUndWo = Global.SafeGetString(oleDbDataReader, 4);

                        var zur = wannUndWo.Replace("~~", "|").Split('|');

                        ZeitUndOrts zeitUndOrts = new ZeitUndOrts();

                        for (int i = 0; i < zur.Length; i++)
                        {
                            if (zur[i] != "")
                            {
                                var zurr = zur[i].Split('~');

                                int tag = 0;
                                int stunde = 0;
                                List<string> raum = new List<string>();

                                try
                                {
                                    tag = Convert.ToInt32(zurr[1]);
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Der Unterricht " + id + " hat keinen Tag.");
                                }

                                try
                                {
                                    stunde = Convert.ToInt32(zurr[2]);
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Der Unterricht " + id + " hat keine Stunde.");
                                }

                                try
                                {
                                    var ra = zurr[3].Split(';');

                                    foreach (var item in ra)
                                    {
                                        if (item != "")
                                        {
                                            raum.AddRange((from r in raums
                                                           where item.Replace(";", "") == r.Id.ToString()
                                                           select r.Raumnummer));
                                        }
                                    }

                                    if (raum.Count == 0)
                                    {
                                        raum.Add("");
                                    }
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Der Unterricht " + id + " hat keinen Raum.");
                                }
                                
                                ZeitUndOrt zeitUndOrt = new ZeitUndOrt(tag, stunde, raum);
                                zeitUndOrts.Add(zeitUndOrt);
                            }
                        }

                        string lessonElement = Global.SafeGetString(oleDbDataReader, 1);

                        int anzahlGekoppelterLehrer = lessonElement.Count(x => x == '~') / 21;

                        List<string> klassenKürzel = new List<string>();

                        for (int i = 0; i < anzahlGekoppelterLehrer; i++)
                        {
                            var lesson = lessonElement.Split(',');

                            var les = lesson[i].Split('~');
                            string lehrer = les[0] == "" ? null : (from l in lehrers where l.Id.ToString() == les[0] select l.Kürzel).FirstOrDefault();

                            string fach = les[2] == "0" ? "" : (from f in fachs where f.Id.ToString() == les[2] select f.KürzelUntis).FirstOrDefault();

                            string raumDiesesUnterrichts = "";
                            if (les[3] != "")
                            {
                                raumDiesesUnterrichts = (from r in raums where (les[3].Split(';')).Contains(r.Id.ToString()) select r.Raumnummer).FirstOrDefault();
                            }

                            int anzahlStunden = oleDbDataReader.GetInt32(2);

                            var unterrichtsgruppeDiesesUnterrichts = (from u in unterrichtsgruppes where u.Id == oleDbDataReader.GetInt32(3) select u).FirstOrDefault();

                            if (les.Count() >= 17)
                            {
                                foreach (var kla in les[17].Split(';'))
                                {
                                    Klasse klasse = new Klasse();

                                    if (kla != "")
                                    {
                                        if (!(from kl in klassenKürzel
                                              where kl == (from k in klasses
                                                           where k.Id == Convert.ToInt32(kla)
                                                           select k.NameUntis).FirstOrDefault()
                                              select kl).Any())
                                        {
                                            klassenKürzel.Add((from k in klasses
                                                               where k.Id == Convert.ToInt32(kla)
                                                               select k.NameUntis).FirstOrDefault());
                                        }
                                    }
                                }
                            }
                            else
                            {
                            }

                            if (lehrer != null)
                            {
                                for (int z = 0; z < zeitUndOrts.Count; z++)
                                {
                                    // Wenn zwei Lehrer gekoppelt sind und zwei Räume zu dieser Stunde gehören, dann werden die Räume entsprechend verteilt.

                                    string r = zeitUndOrts[z].Raum[0];
                                    try
                                    {
                                        if (anzahlGekoppelterLehrer > 1 && zeitUndOrts[z].Raum.Count > 1)
                                        {
                                            r = zeitUndOrts[z].Raum[i];
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        if (anzahlGekoppelterLehrer > 1 && zeitUndOrts[z].Raum.Count > 1)
                                        {
                                            r = zeitUndOrts[z].Raum[0];
                                        }
                                    }

                                    string k = "";

                                    foreach (var item in klassenKürzel)
                                    {
                                        k += item + ",";
                                    }

                                    // Nur wenn der tagDesUnterrichts innerhalb der Befristung stattfindet, wird er angelegt

                                    DateTime von = DateTime.ParseExact((oleDbDataReader.GetInt32(6)).ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                                    DateTime bis = DateTime.ParseExact((oleDbDataReader.GetInt32(7)).ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                                    DateTime tagDesUnterrichts = datumErsterTagDesPrüfZyklus.AddDays(zeitUndOrts[z].Tag - 1);

                                    if (von <= tagDesUnterrichts && tagDesUnterrichts <= bis)
                                    {
                                        // Wenn der Unterricht mit dieser ID bereits existiert

                                        var u = (from un in this
                                                 where un.LehrerKürzel == lehrer
                                                 where un.FachKürzel == fach
                                                 where un.KlasseKürzel == k.TrimEnd(',')
                                                 select un).FirstOrDefault();

                                        if (u != null)
                                        {
                                            u.Wert = u.Wert + 1;
                                        }
                                        else
                                        {
                                            Unterricht unterricht = new Unterricht(
                                            id,
                                            lehrer,
                                            fach,
                                            k.TrimEnd(','),
                                            r,
                                            "",
                                            zeitUndOrts[z].Tag,
                                            zeitUndOrts[z].Stunde,
                                            unterrichtsgruppeDiesesUnterrichts,
                                            datumErsterTagDesPrüfZyklus,
                                            1);
                                            this.Add(unterricht);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    

                    Console.WriteLine(("Unterrichte " + ".".PadRight(this.Count / 150, '.')).PadRight(48, '.') + (" " + this.Count).ToString().PadLeft(4), '.');

                    oleDbDataReader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Fehler beim Unterricht mit der ID " + id + "\n" + ex.ToString());
                    throw new Exception("Fehler beim Unterricht mit der ID " + id + "\n" + ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }

        public void UvdErzeugen()
        {
            try
            {
                List<UvdZeile> uvd = new List<UvdZeile>();
                
                // Alle Untis-Unterrichte (= Unterrichte gruppiert nach Id) werden nacheinander durchlaufen ...

                var untisIdsDistinct = (from u in this select u.Id).Distinct();

                Console.WriteLine("UVD .. ");

                List<string> tkmListe = new List<string> { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };
                
                
                /*
                foreach (var untisId in untisIdsDistinct)
                {
                    List<Schueler> schuelersDiesesUntisUnterrichts = new List<Schueler>();
                    schuelersDiesesUntisUnterrichts.AddRange((from u in this from s in u.Klasse.Schuelers where u.Id == untisId select s).ToList().Distinct().OrderBy(c => c.Gliederung).ThenBy(n => n.Fachklasse));
                    List<Unterricht> untisUnterrichts = (from u in this where u.Id == untisId orderby u.Wert descending select u).Distinct().ToList();
                    List<Klasse> klassesDiesesUntisUnterrichts = (from u in this where u.Id == untisId select u.Klasse).Distinct().ToList();
                    List<string> fachklassenDerSchülerInDiesemUnterricht = new List<string>();
                    fachklassenDerSchülerInDiesemUnterricht.AddRange((from s in schuelersDiesesUntisUnterrichts select s.Fachklasse).Distinct());
                    List<Lehrer> lehrersDiesesUntisUnterrichts = (from u in untisUnterrichts select u.Lehrer).Distinct().ToList();

                    // Unterrichte ohne Schüler werden ignoriert. Evtl. wird die Klasse erst zum Halbjahr gebildet.

                    string meldung = "Unterricht wird ignoriert: " + untisId + "," + klassesDiesesUntisUnterrichts[0].NameAtlantis + "," + fachklassenDerSchülerInDiesemUnterricht.Count + "SuS," + lehrersDiesesUntisUnterrichts[0].Kürzel + ": ";

                    if (schuelersDiesesUntisUnterrichts.Count() == 0 || fachklassenDerSchülerInDiesemUnterricht.Count == 0)
                    {
                        if (schuelersDiesesUntisUnterrichts.Count() == 0)
                        {
                            meldung += "Keine Schüler im Unterricht.";
                        }
                        else
                        {
                            meldung += "Keine Schülerfachklassen im Unterricht.";
                        }
                        Program.Fehlerliste.Add(new Fehler("UVD", meldung));
                        Console.WriteLine(meldung);
                    }
                    if (schuelersDiesesUntisUnterrichts.Count() > 0)
                    {
                        // Je nachdem, ob die Zahl der Lehrer oder die Zahl der verschiednenen Fachklassen größer ist, wird die Schleife entsprechend oft durchlaufen

                        int anzahlUvdZeilen = Math.Max(Math.Max(fachklassenDerSchülerInDiesemUnterricht.Count, lehrersDiesesUntisUnterrichts.Count), untisUnterrichts.Count);

                        for (int i = 0; i < anzahlUvdZeilen; i++)
                        {
                            string lehrerKürzel = untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)].Lehrer == null ? "" : untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)].Lehrer.Kürzel;
                            string fachStatistikname = untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)].Fach == null ? "" : untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)].Fach.Statistikname;
                            string klasseAtlantisname = untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)].Klasse == null ? "" : untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)].Klasse.NameAtlantis;
                            double wert = untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)] == null ? 0 : untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)].Wert;
                            string tkm = "";
                            int schGes = 0;
                            int schWeiblich = 0;

                            List<string> fachklassenDerSchülerInDieserKlasse = (from u in this from uu in u.Klasse.Schuelers where u.Klasse.NameAtlantis == klasseAtlantisname where u.Id == untisId orderby uu.Fachklasse select uu.Fachklasse).Distinct().ToList();

                            if (fachklassenDerSchülerInDieserKlasse.Count == 0)
                            {
                                Console.WriteLine("Fehler, da diese Klassse keine SuS enthält..");
                            }

                            if (anzahlUvdZeilen > 1)
                            {
                                // Nur wenn es mehrere Fachklassen in einer Klasse gibt, 
                                tkm = tkmListe[Math.Min(i, fachklassenDerSchülerInDieserKlasse.Count - 1)];

                                schGes = (from k in klassesDiesesUntisUnterrichts
                                          from s in k.Schuelers
                                          where k.NameAtlantis == untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)].Klasse.NameAtlantis
                                          where s.Fachklasse == fachklassenDerSchülerInDieserKlasse[Math.Min(i, fachklassenDerSchülerInDieserKlasse.Count - 1)].ToString()
                                          select s).Distinct().Count();

                                schWeiblich = (from k in klassesDiesesUntisUnterrichts
                                               from s in k.Schuelers
                                               where s.Geschlecht34 == "W"
                                               where k.NameAtlantis == untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)].Klasse.NameAtlantis
                                               where s.Fachklasse == fachklassenDerSchülerInDieserKlasse[Math.Min(i, fachklassenDerSchülerInDieserKlasse.Count - 1)].ToString()
                                               select s).Distinct().Count();

                                // Für Folgezeilen (i > 0) wird geprüft ...

                                if (i > 0)
                                {
                                    // ... ob die letzte Zeile zuvor mit nichtleerem Lehrer, den selben Lehrer enthält ...

                                    if ((from u in uvd where u.uvdLehrer != "" select u.uvdLehrer).Last() == lehrerKürzel)
                                    {
                                        // ... falls ja, dann bleiben Lehrer und Fachname leer.

                                        lehrerKürzel = "";
                                        fachStatistikname = "";
                                        wert = 0;
                                    }
                                }
                            }

                            // Aufrunden, da die UVD nur Ganzzahlen versteht.

                            if ((wert % 1) != 0 && wert > 0)
                            {
                                wert = Math.Ceiling(wert);
                                Program.Fehlerliste.Add(new Fehler("UVD", "Unterricht " + untisId + " aufgerundet auf " + wert + "."));
                            }

                            // Hauptzeilen ohne Wert werden ignoriert.

                            if ((wert == 0 && lehrerKürzel != "" && fachStatistikname != "") || lehrerKürzel == "LAT" || lehrerKürzel == "?")
                            {
                                Program.Fehlerliste.Add(new Fehler("UVD", "Unterricht " + untisId + " wird ignoriert: Lehrer: " + lehrerKürzel.PadRight(4) + " Klasse:" + klasseAtlantisname.PadRight(5) + " Fach:" + fachStatistikname.PadRight(5) + " Wert:" + wert));

                                if ((wert == 0 && lehrerKürzel != "" && fachStatistikname != ""))
                                {
                                    while (i <= anzahlUvdZeilen)
                                    {
                                        string ilehrerId = untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)].Lehrer.Kürzel;
                                        string ifachStatistikname = untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)].Fach.Statistikname;
                                        string iklasseAtlantisname = untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)].Klasse.NameAtlantis;
                                        double iwert = untisUnterrichts[Math.Min(i, untisUnterrichts.Count - 1)].Wert;
                                        Program.Fehlerliste.Add(new Fehler("UVD", "Unterricht " + untisId + " wird ignoriert: Lehrer: " + ilehrerId.PadRight(4) + " Klasse:" + iklasseAtlantisname.PadRight(5) + " Fach:" + ifachStatistikname.PadRight(5) + " Wert:" + iwert));
                                        i++;
                                    }
                                }
                            }
                            else
                            {
                                if (uvd.Count() % 100 == 0)
                                {
                                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                                    Program.ClearCurrentConsoleLine();
                                    Console.WriteLine("UVD " + ".".PadRight(uvd.Count / 30, '.') + " " + (uvd.Count - 1) + ".");
                                }
                                uvd.Add(new UvdZeile(klasseAtlantisname, tkm, "", "", wert, fachStatistikname, lehrerKürzel, schGes, schWeiblich, "", "", "", "BM", "0.0.1", 7, ""));
                            }
                        }
                    }
                }

                Console.SetCursorPosition(0, Console.CursorTop - 1);
                Program.ClearCurrentConsoleLine();
                Console.WriteLine(("UVD " + ".".PadRight(uvd.Count / 30, '.')).PadRight(60, '.') + " " + uvd.Count
                    + ((from f in Program.Fehlerliste where f.Kategorie == "UVD" select f).Any() ? " (Fehlerliste beachten!)" : ""));

                // Datei generieren

                var x = (from u in uvd
                         select new
                         {
                             Klasse = u.uvdKlasse,
                             TKM = u.uvdTKM,
                             Jahrg = u.uvdJahrg,
                             Kursart = u.uvdKursart,
                             WoStd = u.uvdWoStd,
                             Fach = u.uvdFach,
                             Lehrer = u.uvdLehrer,
                             SchGes = u.uvdSchGes,
                             SchWeibl = u.uvdSchWeibl,
                             USprache = u.uvdUSprache,
                             Fremde = u.uvdFremde,
                             Bildungsgang = u.uvdBildungsgang,
                             Produktname = u.uvdProduktname,
                             Produktversion = u.uvdProduktversion
                         }
                                        );

                Global.CsvAusgabe(ini, "UVD.txt", x, Encoding.Default, '"');
                Global.CsvAusgabe(ini, "UVDfehler.txt", new List<string>(), Encoding.Default, '"');*/
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        internal List<Unterricht> SortierenUndKumulieren()
        {
            try
            {
                // Die Unterrichte werden chronologisch sortiert

                List<Unterricht> sortierteUnterrichts = (from u in this
                                                         orderby u.KlasseKürzel, u.FachKürzel, u.Raum, u.Tag, u.Stunde
                                                         select u).ToList();

                for (int i = 0; i < sortierteUnterrichts.Count; i++)
                {
                    // Wenn es einen nachfolgenden Unterricht gibt ...

                    if (i < sortierteUnterrichts.Count - 1)
                    {
                        // ... und dieser in allen Eigenschaften identisch ist ...

                        if (sortierteUnterrichts[i].KlasseKürzel == sortierteUnterrichts[i + 1].KlasseKürzel && sortierteUnterrichts[i].FachKürzel == sortierteUnterrichts[i + 1].FachKürzel && sortierteUnterrichts[i].Raum == sortierteUnterrichts[i + 1].Raum)
                        {
                            // ... und der nachfolgende Unterricht unmittelbar (nach der Pause) anschließt ... 

                            if (sortierteUnterrichts[i].Bis == sortierteUnterrichts[i + 1].Von || sortierteUnterrichts[i].Bis.AddMinutes(15) == sortierteUnterrichts[i + 1].Von || sortierteUnterrichts[i].Bis.AddMinutes(20) == sortierteUnterrichts[i + 1].Von)
                            {
                                // ... wird der Beginn des Nachfolgers nach vorne geschoben ...

                                sortierteUnterrichts[i + 1].Von = sortierteUnterrichts[i].Von;

                                // ... und der Vorgänger wird gelöscht.

                                sortierteUnterrichts.RemoveAt(i);

                                // Der Nachfolger bekommt den Index des Vorgängers.
                                i--;
                            }
                        }
                    }
                }
                return (
                    from s in sortierteUnterrichts
                    orderby s.Tag, s.Stunde, s.KlasseKürzel
                    select s).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }
        }
                
        public Unterrichts Kumulieren()
        {
            try
            {
                for (int i = 0; i < this.Count; i++)
                {
                    // Wenn es einen nachfolgenden Unterricht gibt ...

                    if (i < this.Count - 1)
                    {
                        // ... und dieser in allen Eigenschaften identisch ist ...

                        if (this[i].KlasseKürzel == this[i + 1].KlasseKürzel && this[i].FachKürzel == this[i + 1].FachKürzel && this[i].Raum == this[i + 1].Raum)
                        {
                            // ... und der nachfolgende Unterricht unmittelbar (nach der Pause) anschließt ... 

                            if (this[i].Bis == this[i + 1].Von || this[i].Bis.AddMinutes(15) == this[i + 1].Von || this[i].Bis.AddMinutes(20) == this[i + 1].Von)
                            {
                                // ... wird der Beginn des Nachfolgers nach vorne geschoben ...

                                this[i + 1].Von = this[i].Von;

                                // ... und der Vorgänger wird gelöscht.

                                this.RemoveAt(i);

                                // Der Nachfolger bekommt den Index des Vorgängers.
                                i--;
                            }
                        }
                    }
                }
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }
        }
    }
}