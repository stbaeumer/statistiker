﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace statistiker
{
    class Program
    {
        public const string connectionString = @"Provider = Microsoft.Jet.OLEDB.4.0; Data Source=M:\\Data\\gpUntis.mdb;";
        private static Lehrers lehrers;

        static void Main(string[] args)
        {
            List<string> aktSj = new List<string>
            {
                (DateTime.Now.Month >= 8 ? DateTime.Now.Year : DateTime.Now.Year - 1).ToString(),
                (DateTime.Now.Month >= 8 ? DateTime.Now.Year + 1 : DateTime.Now.Year).ToString()
            };

            Console.WriteLine("Atlantis2Webuntis (Version 20190914)");
            Console.WriteLine("====================================");
            Console.WriteLine("");
            
            try
            {
                CultureInfo CUI = CultureInfo.CurrentCulture;
                var kalenderwoche = CUI.Calendar.GetWeekOfYear(DateTime.Now, CUI.DateTimeFormat.CalendarWeekRule, CUI.DateTimeFormat.FirstDayOfWeek);
                                
                var datumMontagDerKalenderwoche = GetMondayDateOfWeek(kalenderwoche, Convert.ToInt32(kalenderwoche < 30 ? aktSj[1] : aktSj[0]));
                
                Periodes periodes = new Periodes(
                        aktSj[0] + aktSj[1],
                        connectionString);

                int periode = periodes.GetPeriode(datumMontagDerKalenderwoche);

                Fachs fachs = new Fachs(
                    aktSj[0] + aktSj[1],
                    connectionString);

                Unterrichtsgruppes unterrichtsgruppes = new Unterrichtsgruppes(
                    aktSj[0] + aktSj[1],
                    connectionString);

                lehrers = new Lehrers(
                    aktSj[0] + aktSj[1],
                    connectionString,
                    periodes);

                Klasses klasses = new Klasses(
                                    aktSj[0] + aktSj[1],
                                    lehrers,
                                    connectionString,
                                    periode);

                Raums raums = new Raums(
                    aktSj[0] + aktSj[1],
                    connectionString,
                    periode);

                Unterrichts unterrichts = new Unterrichts(
                    aktSj[0] + aktSj[1],
                    datumMontagDerKalenderwoche,
                    connectionString,
                    periode,
                    klasses,
                    lehrers,
                    fachs,
                    raums,
                    unterrichtsgruppes);
                                
                unterrichts.UvdErzeugen();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                Console.ReadKey();
            }
        }
        
        private static DateTime GetMondayDateOfWeek(int week, int year)
        {
            int i = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(new DateTime(year, 1, 1), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            if (i == 1)
            {
                return CultureInfo.CurrentCulture.Calendar.AddDays(new DateTime(year, 1, 1), ((week - 1) * 7 - GetDayCountFromMonday(CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(new DateTime(year, 1, 1))) + 1));
            }
            else
            {
                int x = Convert.ToInt32(CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(new DateTime(year, 1, 1)));
                return CultureInfo.CurrentCulture.Calendar.AddDays(new DateTime(year, 1, 1), ((week - 1) * 7 + (7 - GetDayCountFromMonday(CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(new DateTime(year, 1, 1)))) + 1));
            }
        }

        private static int GetDayCountFromMonday(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    return 1;
                case DayOfWeek.Tuesday:
                    return 2;
                case DayOfWeek.Wednesday:
                    return 3;
                case DayOfWeek.Thursday:
                    return 4;
                case DayOfWeek.Friday:
                    return 5;
                case DayOfWeek.Saturday:
                    return 6;
                default:
                    //Sunday
                    return 7;
            }
        }
    }
}
