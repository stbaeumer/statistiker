﻿namespace statistiker
{
    public class Lehrer
    {
        public int Id { get; internal set; }
        public string Kürzel { get; internal set; }
        public string Mail { get; internal set; }        
    }
}