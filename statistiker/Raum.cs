﻿namespace statistiker
{
    public class Raum
    {
        public int Id { get; internal set; }
        public string Raumnummer { get; internal set; }
    }
}