﻿namespace statistiker
{
    public class Fach
    {
        public int Id { get; internal set; }
        public string KürzelUntis { get; internal set; }
        public string LangnameUntis { get; internal set; }
        public string BezeichnungImZeugnis { get; internal set; }
        public string Fachklassen { get; internal set; }
    }
}