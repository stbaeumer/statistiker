﻿using System;
using System.Data.OleDb;

namespace statistiker
{
    internal class Global
    {
        public static string SafeGetString(OleDbDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            return string.Empty;
        }
    }
}