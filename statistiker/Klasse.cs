﻿using System.Collections.Generic;

namespace statistiker
{
    public class Klasse
    {
        public int Id { get; internal set; }
        public string NameUntis { get; internal set; }
        public List<Lehrer> Klassenleitungen { get; internal set; }
    }
}